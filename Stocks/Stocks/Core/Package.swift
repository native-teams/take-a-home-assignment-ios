// swift-tools-version: 5.8
import PackageDescription

let package = Package(
    name: "Core",
    defaultLocalization: "en",
    platforms: [.iOS(.v16)],
    products: [
        .library(
            name: "Integration",
            targets: ["Integration"])
    ],
    dependencies: [],
    targets: [
        .target(
            name: "Domain",
            dependencies: []),
        .target(
            name: "Integration",
            dependencies: ["Domain", "Interface"]),
        .target(
            name: "Interface",
            dependencies: []),
        .testTarget(
            name: "DomainTests",
            dependencies: [
                "Domain"
            ]),
        .testTarget(
            name: "IntegrationTests",
            dependencies: [
                "Integration"
            ])
    ]
)
