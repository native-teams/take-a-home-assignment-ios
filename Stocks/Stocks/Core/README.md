# Core

Core package that contains the app's modules.
  
## Interface
The app “frontend” logic. This where the screens, components and other UI stuff live.

## Domain
The app “backend” logic. This includes IO (networking, storage, etc.) as well as business logic (non-UI state management).

## Integration 
This package contains the “glue” that connects the app’s UI to its core logic.

Tip: 
For model objects, we will have separate versions for the business logic and for the UI.

![module dependencies](../../Core.png)
