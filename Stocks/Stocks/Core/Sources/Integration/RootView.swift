import SwiftUI
import Domain
import Interface

public typealias ApplicationModel = Domain.ApplicationModel

public struct RootView: View {
    private var model: ApplicationModel
    
    public init(model: ApplicationModel) {
        self.model = model
    }
    
    public var body: some View {
        StocksView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        RootView(model: .init())
    }
}
